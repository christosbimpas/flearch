//
//  CustomCollectionViewCell.h
//  Unmuddled
//
//  Created by Christos  Bimpas on 5/11/2015.
//  Copyright © 2015 pixelbirth. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionViewCell : UICollectionViewCell

@property(nonatomic,strong) UIImageView *imageView;



- (void)setCellWithImageUrl:(NSURL*)imageUrl size:(float)cellSize andGenericImage:(NSString*)imageName;
@end
