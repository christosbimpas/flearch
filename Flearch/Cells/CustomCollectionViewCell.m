//
//  CustomCollectionViewCell.m
//  Unmuddled
//
//  Created by Christos  Bimpas on 5/11/2015.
//  Copyright © 2015 pixelbirth. All rights reserved.
//

#import "CustomCollectionViewCell.h"
#import "global.h"
#import <SDWebImage/UIImageView+WebCache.h>
@implementation CustomCollectionViewCell
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        
        
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
        [self.imageView setClipsToBounds:YES];
        self.imageView.layer.borderColor = [UIColor grayColor].CGColor;
        self.imageView.layer.borderWidth = 0.5;
        self.imageView.contentMode = UIViewContentModeCenter;
        [self addSubview:self.imageView];
        
       
        
       

    }
    return self;
}

- (void)setCellWithImageUrl:(NSURL*)imageUrl size:(float)cellSize andGenericImage:(NSString*)imageName {
    
    
    
    self.imageView.frame = CGRectMake(0, 0, cellSize, cellSize);
    
    [self.imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:imageName] options:SDWebImageRefreshCached];
    

    
    
   
    
    
    
    
}
@end
