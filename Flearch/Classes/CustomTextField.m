//
//  CustomTextField.m
//  Flearch
//
//  Created by Christos  Bimpas on 11/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField


- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    return CGRectZero;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-20, 2, 19/1.5, 25/1.5)];
    arrowImageView.image = [UIImage imageNamed:@"dropdownArrow"];
    [self addSubview:arrowImageView];
    
   
}


@end
