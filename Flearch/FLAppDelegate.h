//
//  AppDelegate.h
//  Flearch
//
//  Created by Christos  Bimpas on 10/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

