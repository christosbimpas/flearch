//
//  PhotoDetailsViewController.h
//  Flearch
//
//  Created by Christos  Bimpas on 11/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FLPhotoDetailsViewController : UIViewController <UITableViewDataSource,UITableViewDelegate>

@property(strong,nonatomic) NSString *photoId;
@property(strong,nonatomic) NSURL *photoUrl;
@property(strong,nonatomic)NSMutableDictionary *photoInfoDictionary;
@property(strong,nonatomic) UITableView *myTableView;
@property(assign) int tableCount;
@property(strong,nonatomic) UIImage *mainImage;
@property(strong,nonatomic) NSString *contentString;
@property(strong,nonatomic) NSString *titleString;
@property(strong,nonatomic) NSString *desctiptionString;
@property(strong,nonatomic) UIActivityIndicatorView *myActivityIndicator;
@property(strong,nonatomic) NSArray *tagsArray;
@property(strong,nonatomic) NSArray *commentsArray;
@property(strong,nonatomic)NSMutableArray *cellViewsArray;

@end
