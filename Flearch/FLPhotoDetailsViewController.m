//
//  PhotoDetailsViewController.m
//  Flearch
//
//  Created by Christos  Bimpas on 11/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import "FLPhotoDetailsViewController.h"
#import "FLSearchResultsViewController.h"
#import "FlickrKit.h"
#import "global.h"
#import <MapKit/MapKit.h>
@interface FLPhotoDetailsViewController ()

@end

@implementation FLPhotoDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initialiseProperties];
    [self createUI];
    [self getImageInfo];
   
    
    
}

-(void)initialiseProperties {
    
    self.tableCount = 0;
   
    self.cellViewsArray = [[NSMutableArray alloc] init];
}

-(void)createUI {
    
    UIImageView *myImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    myImageView.image = [UIImage imageNamed:@"headerImage"];
    myImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = myImageView;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-4],
                                    NSFontAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.tintColor = NAVIGATION_BACK_BUTTON_COLOR;
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor = NAVIGATION_COLOR;
    
    
    UIBarButtonItem *shareButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                                                                 target:self
                                                                                 action:@selector(showShareSheet)];
    self.navigationItem.rightBarButtonItem = shareButton;
    
    
    self.myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height-10)];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    self.myTableView.hidden = YES;
    [self.view addSubview:self.myTableView];
    
    
    
    self.myActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.myActivityIndicator.center = self.view.center;
    self.myActivityIndicator.layer.zPosition = 1000;
    
    [self.view addSubview:self.myActivityIndicator];
    [self.myActivityIndicator startAnimating];
}

-(void)getComments {
    
    NSDictionary *commentsDictionary = [[NSDictionary alloc] initWithDictionary:[self.photoInfoDictionary objectForKey:@"comments"]];
    NSString *commentsTotalString = [commentsDictionary objectForKey:@"_content"];
     DLog(@"commentsTotalString %@",commentsTotalString);
    int commentsTotal = [commentsTotalString intValue];
    
    if(commentsTotal>0) {
    FKFlickrPhotosCommentsGetList *comments = [[FKFlickrPhotosCommentsGetList alloc] init];
    comments.photo_id = self.photoId;
    
    [[FlickrKit sharedFlickrKit] call:comments completion:^(NSDictionary *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (response) {
                
                
                
                
                self.commentsArray = [[NSArray alloc] initWithArray:[[response objectForKey:@"comments"] objectForKey:@"comment"]];
                DLog(@"commentsArray %@",self.commentsArray);
                [self calculateTableElements];
            }
            
            else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                
            }
        });
        
    }];
    }
    
    else {
        
         [self calculateTableElements];
    }
}

-(void)calculateTableElements {
    
   
    //image
    self.mainImage = [UIImage imageWithData: [NSData dataWithContentsOfURL:self.photoUrl]];
    DLog(@"image height %f% f",self.mainImage.size.width,self.mainImage.size.height);
    float cellImageHeight = self.view.frame.size.width*self.mainImage.size.height/self.mainImage.size.width;
    if(cellImageHeight>0) {
       
        UIView *cellImageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, cellImageHeight)];
        UIImageView *mainImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, self.view.frame.size.width-10, cellImageHeight-10)];
        mainImageView.image = self.mainImage;
        mainImageView.contentMode = UIViewContentModeScaleAspectFit;
        [cellImageView addSubview:mainImageView];
        [self.cellViewsArray addObject:cellImageView];
    }
    ///title
    NSDictionary *titleDictionary = [[NSDictionary alloc] initWithDictionary:[self.photoInfoDictionary objectForKey:@"title"]];
    
    if([[titleDictionary objectForKey:@"_content"] isKindOfClass:[NSString class]]) {
       self.titleString = [titleDictionary objectForKey:@"_content"];
        if(self.titleString.length>0) {
        UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 20)];
        titleLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-2];
        titleLabel.text = @"Title";
        titleLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [titleView addSubview:titleLabel];
        
        
        UITextView *titleTextView = [[UITextView alloc] initWithFrame:CGRectMake(6, 30, self.view.frame.size.width-20, 160)];
        titleTextView.font =  [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-3];
        titleTextView.textColor = BUTTON_TITLE_DARK_COLOR;
        titleTextView.backgroundColor = [UIColor clearColor];
        titleTextView.editable = NO;
        titleTextView.text = self.titleString;
        [titleTextView sizeToFit];
        [titleTextView layoutIfNeeded];
      
        CGRect frame = titleTextView.frame;
        frame.size.height = titleTextView.contentSize.height;
        titleTextView.frame = frame;
        [titleView addSubview:titleTextView];

         titleView.frame = CGRectMake(0, 0, self.view.frame.size.width, 40+frame.size.height);
        [self.cellViewsArray addObject:titleView];
        }
        
    }
    
    ///description
    
    NSDictionary *descriptionDictionary = [[NSDictionary alloc] initWithDictionary:[self.photoInfoDictionary objectForKey:@"description"]];
    
    if([[descriptionDictionary objectForKey:@"_content"] isKindOfClass:[NSString class]]) {
        NSString *descriptionString = [descriptionDictionary objectForKey:@"_content"];
        if(descriptionString.length>0) {

        UIView *descriptionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
        UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 20)];
        descriptionLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-2];
        descriptionLabel.text = @"Description";
        descriptionLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [descriptionView addSubview:descriptionLabel];
        
        UITextView *descriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(6, 30, self.view.frame.size.width-20, 160)];
        descriptionTextView.font =  [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-3];
        descriptionTextView.textColor = BUTTON_TITLE_DARK_COLOR;
        descriptionTextView.backgroundColor = [UIColor clearColor];
        descriptionTextView.userInteractionEnabled = NO;
        NSString *htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %dpx\">%@</span>",FONT_LIGHT,FONT_LIGHT_SIZE-6, descriptionString];
        NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        descriptionTextView.attributedText = attributedString;
//        descriptionTextView.text = descriptionString;
        [descriptionTextView sizeToFit];
        [descriptionTextView layoutIfNeeded];
        
        CGRect frame = descriptionTextView.frame;
        frame.size.height = descriptionTextView.contentSize.height;
        descriptionTextView.frame = frame;
        [descriptionView addSubview:descriptionTextView];
        
        descriptionView.frame = CGRectMake(0, 0, self.view.frame.size.width, 40+frame.size.height);
        [self.cellViewsArray addObject:descriptionView];

        }
    }
    
    //location
    NSDictionary *locationDictionary;
    if([[self.photoInfoDictionary objectForKey:@"location"] isKindOfClass:[NSDictionary class]]) {
        locationDictionary = [[NSDictionary alloc] initWithDictionary:[self.photoInfoDictionary objectForKey:@"location"]];
        
        
         UIView *locationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 360)];
        
        UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 20)];
        locationLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-2];
        locationLabel.text = @"Location";
        locationLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [locationView addSubview:locationLabel];
        
        
        UILabel *theLocationLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 35, self.view.frame.size.width-20, 20)];
        theLocationLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-3];
        theLocationLabel.text = [NSString stringWithFormat:@"%@, %@, %@",[[locationDictionary objectForKey:@"locality"] objectForKey:@"_content"],[[locationDictionary objectForKey:@"county"] objectForKey:@"_content"],[[locationDictionary objectForKey:@"country"] objectForKey:@"_content"]];
        theLocationLabel.adjustsFontSizeToFitWidth = YES;
        theLocationLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [locationView addSubview:theLocationLabel];
        
        
        
        NSString *latitudeString = [locationDictionary objectForKey:@"latitude"];
        NSString *longitudeString = [locationDictionary objectForKey:@"longitude"];
        
        double lat = [latitudeString doubleValue];
        double lon = [longitudeString doubleValue];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(lat, lon);
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = position;
        annotation.title = [[locationDictionary objectForKey:@"country"] objectForKey:@"_content"];
        annotation.subtitle = [[locationDictionary objectForKey:@"county"] objectForKey:@"_content"];
        MKMapView *theMapView = [[MKMapView alloc] initWithFrame:CGRectMake(10, 60, self.view.frame.size.width-20, 280) ];
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        span.latitudeDelta = 0.03;
        span.longitudeDelta = 0.03;
        region.span = span;
        region.center = position;
        [theMapView addAnnotation:annotation];
        [theMapView setRegion:region animated:YES];
        
        
        [locationView addSubview:theMapView];
        [self.cellViewsArray addObject:locationView];
    }
    
    ///owner
    NSDictionary *ownerDictionary;
    
    if([[self.photoInfoDictionary objectForKey:@"owner"] isKindOfClass:[NSDictionary class]]) {
        ownerDictionary = [[NSDictionary alloc] initWithDictionary:[self.photoInfoDictionary objectForKey:@"owner"]];
        NSString *realNameString = [ownerDictionary objectForKey:@"realname"];
        NSString *userNameString = [ownerDictionary objectForKey:@"username"];
        
        
        int yOffset = 40;
        
        UIView *ownerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
        
       
        UILabel *ownerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 20)];
        ownerLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-2];
        ownerLabel.text = @"Owner";
        ownerLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [ownerView addSubview:ownerLabel];
        
        
        if(realNameString.length>0) {
        UILabel *realNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, yOffset, self.view.frame.size.width-20, 20)];
        realNameLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-3];
        realNameLabel.text = [NSString stringWithFormat:@"Name: %@",realNameString];
       
        realNameLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [ownerView addSubview:realNameLabel];
            yOffset +=30;
        }
        
        if(userNameString.length>0) {
        UILabel *usernameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, yOffset, self.view.frame.size.width-20, 20)];
        usernameLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-3];
        usernameLabel.text = [NSString stringWithFormat:@"Username: %@",userNameString];
        
        usernameLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [ownerView addSubview:usernameLabel];
            yOffset+=30;
            
        }
        if(yOffset>40) {
            
        ownerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 10+yOffset);

        [self.cellViewsArray addObject:ownerView];
        }
        
        
    }
    
    
   
    ///tags
    
    NSDictionary *tagsDictionary = [[NSDictionary alloc] initWithDictionary:[self.photoInfoDictionary objectForKey:@"tags"]];
    
    if([[tagsDictionary objectForKey:@"tag"] isKindOfClass:[NSArray class]]) {
        
        
            self.tagsArray = [[NSArray alloc] initWithArray:[tagsDictionary objectForKey:@"tag"]];
        if([self.tagsArray count]>0) {
            UIView *tagsView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
            
            UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 20)];
            tagLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-2];
            tagLabel.text = @"Tags";
            tagLabel.textColor = BUTTON_TITLE_DARK_COLOR;
            [tagsView addSubview:tagLabel];
            int xOffset = 10;
            int yOffset = 40;
        
            for(int i=0;i<[self.tagsArray count];i++) {
            NSString *tagString = [self.tagsArray[i] objectForKey:@"_content"];
            float desiredButonWidth = [self widthOfString:tagString]+10;
            if(xOffset+desiredButonWidth>self.view.frame.size.width) {
                
                yOffset += 40;
                xOffset = 10;
                }
                
            UIButton *tagButton = [[UIButton alloc] initWithFrame:CGRectMake(xOffset, yOffset, desiredButonWidth, 30)];
            tagButton.titleLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-2];
            [tagButton setTitle:tagString forState:UIControlStateNormal];
            tagButton.layer.borderColor = BUTTON_TITLE_DARK_COLOR.CGColor;
            tagButton.tag = i;
                [tagButton addTarget:self action:@selector(tagButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            tagButton.layer.borderWidth = 1;
            [tagButton setTitleColor:BUTTON_TITLE_DARK_COLOR forState:UIControlStateNormal];
            xOffset += desiredButonWidth + 10;
            [tagsView addSubview:tagButton];
                }
        
            tagsView.frame = CGRectMake(0, 0, self.view.frame.size.width, 40 + yOffset);
            [self.cellViewsArray addObject:tagsView];
        }
       
    }

   
    ///dates
    
    NSDictionary *datesDictionary;
    
    if([[self.photoInfoDictionary objectForKey:@"dates"] isKindOfClass:[NSDictionary class]]) {
        datesDictionary = [[NSDictionary alloc] initWithDictionary:[self.photoInfoDictionary objectForKey:@"dates"]];
       
        double postedTimeStamp = [[datesDictionary objectForKey:@"posted"] doubleValue];
        NSDate *postedDate =  [NSDate dateWithTimeIntervalSince1970:postedTimeStamp];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *postedDateString = [formatter stringFromDate:postedDate];
        
       
        
      
        NSString *takenDateString = [datesDictionary objectForKey:@"taken"];
        DLog(@"date taken %@",takenDateString);
    
        UIView *datesView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 130)];
        
        UILabel *postedDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 20)];
        postedDateLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-2];
        postedDateLabel.text = @"Posted";
        postedDateLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [datesView addSubview:postedDateLabel];
        
        UILabel *thePostedDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 40, self.view.frame.size.width-20, 20)];
        thePostedDateLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-3];
        thePostedDateLabel.text = postedDateString;
        thePostedDateLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [datesView addSubview:thePostedDateLabel];
        
        
        UILabel *takenDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, self.view.frame.size.width-20, 20)];
        takenDateLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-2];
        takenDateLabel.text = @"Taken";
        takenDateLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [datesView addSubview:takenDateLabel];

        
        UILabel *theTakenDateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, self.view.frame.size.width-20, 20)];
        theTakenDateLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-3];
        theTakenDateLabel.text = takenDateString;
        theTakenDateLabel.textColor = BUTTON_TITLE_DARK_COLOR;
        [datesView addSubview:theTakenDateLabel];
            
        
        [self.cellViewsArray addObject:datesView];
            
       
    }
    
    
    ///comments
    if(self.commentsArray.count>0) {
    UIView *commentTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 40)];
    UILabel *commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-20, 20)];
    commentLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-2];
    commentLabel.text = @"Comments";
    commentLabel.textColor = BUTTON_TITLE_DARK_COLOR;
    [commentTitleView addSubview:commentLabel];
     [self.cellViewsArray addObject:commentTitleView];
    }
    for(int i=(int)[self.commentsArray count]-1;i>=0;i--) {
        
        
        NSString *commentString = [self.commentsArray[i] objectForKey:@"_content"];
        NSString *authorNameString = [self.commentsArray[i] objectForKey:@"authorname"];
        double dateCreatedTimeStamp = [[self.commentsArray[i] objectForKey:@"datecreate"] doubleValue];
        NSDate *dateCreated = [NSDate dateWithTimeIntervalSince1970:dateCreatedTimeStamp];
        NSTimeInterval secondsBetween = [dateCreated timeIntervalSinceDate:[NSDate date]];
        NSString *hoursOrMinutesString;
        hoursOrMinutesString = @"h";
        
        //hours
        int timeDifference = secondsBetween / 86400 / 24;
        
        
        //minutes
        if(timeDifference == 0) {
            
            timeDifference = secondsBetween / 60;
            hoursOrMinutesString = @"m";
        }
        //seconds
        if(timeDifference == 0) {
            
            timeDifference = secondsBetween;
            hoursOrMinutesString = @"s";
        }
        
        timeDifference = abs(timeDifference);
        if(commentString.length>0) {
            
            UIView *commentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
            UILabel *authorLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width-50, 20)];
            authorLabel.font = [UIFont fontWithName:FONT_BOLD size:FONT_BOLD_SIZE-4];
            authorLabel.text = authorNameString;
            authorLabel.textColor = BUTTON_TITLE_DARK_COLOR;
            [commentView addSubview:authorLabel];
            
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-50,10 , 40, 20)];
            timeLabel.textAlignment = NSTextAlignmentRight;
            timeLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-6];
            timeLabel.text = [NSString stringWithFormat:@"%d%@",timeDifference,hoursOrMinutesString];
            timeLabel.textColor = [UIColor grayColor];
            [commentView addSubview:timeLabel];


            
            UITextView *commentTextView = [[UITextView alloc] initWithFrame:CGRectMake(6, 30, self.view.frame.size.width-20, 160)];
            commentTextView.font =  [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-3];
            commentTextView.textColor = BUTTON_TITLE_DARK_COLOR;
            commentTextView.backgroundColor = [UIColor clearColor];
            commentTextView.editable = NO;
            NSString *htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %dpx\">%@</span>",FONT_LIGHT,FONT_LIGHT_SIZE-6, commentString];
            NSAttributedString *attributedString = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
            commentTextView.attributedText = attributedString;
           
            [commentTextView sizeToFit];
            [commentTextView layoutIfNeeded];
            
            CGRect frame = commentTextView.frame;
            frame.size.height = commentTextView.contentSize.height;
            commentTextView.frame = frame;
            [commentView addSubview:commentTextView];
            
            commentView.frame = CGRectMake(0, 0, self.view.frame.size.width, 40+frame.size.height);
            [self.cellViewsArray addObject:commentView];
            
        }
    }
  
    
    
    [self.myActivityIndicator stopAnimating];
    self.myTableView.hidden = NO;
    [self.myTableView reloadData];

    
    
}

-(void)showShareSheet {
    
    
    NSDictionary *urlsDictionary = [[NSDictionary alloc] initWithDictionary:[self.photoInfoDictionary objectForKey:@"urls"]];
    NSArray *urlArray = [[NSArray alloc] initWithArray:[urlsDictionary objectForKey:@"url"]];
    
   
    NSString *textToShare = self.titleString;
    UIImage *imageToShare = self.mainImage;
    NSMutableArray *itemsToShare = [[NSMutableArray alloc] initWithObjects:textToShare,imageToShare, nil];
    for(int i=0;i<[urlArray count];i++ ) {
        
        [itemsToShare addObject:[urlArray[i] objectForKey:@"_content" ]];
    }
 
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[ UIActivityTypeAssignToContact];
    [self presentViewController:activityVC animated:YES completion:nil];
}


-(void)getImageInfo {
    
    FKFlickrPhotosGetInfo *info = [[FKFlickrPhotosGetInfo alloc] init];
    info.photo_id = self.photoId;
    
    [[FlickrKit sharedFlickrKit] call:info completion:^(NSDictionary *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (response) {
                if(self.photoInfoDictionary == nil) {
                    self.photoInfoDictionary = [[NSMutableDictionary alloc] init];
                }
                self.photoInfoDictionary = [response valueForKeyPath:@"photo"];
                DLog(@"image info reponse is %@", self.photoInfoDictionary);
                 [self getComments];
                
               
                
                
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        });
    }];
    
    
}

#pragma mark TABLEVIEW DELEGATE

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [self.cellViewsArray count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    NSString *CellIdentifier = [NSString stringWithFormat:@"S%1ldR%1ld",(long)indexPath.section,(long)indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *cellView = (UIView*)self.cellViewsArray[indexPath.row];
    [cell addSubview:cellView];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UIView *cellView = (UIView*)self.cellViewsArray[indexPath.row];
    return  cellView.frame.size.height;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark FUNCTIONS

- (CGFloat)widthOfString:(NSString *)string  {
    UIFont *font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-2];
    NSDictionary *userAttributes = @{NSFontAttributeName: font,
                                     NSForegroundColorAttributeName: [UIColor blackColor]};
   
   
    const CGSize textSize = [string sizeWithAttributes: userAttributes];
    
    return textSize.width;
}


-(void)tagButtonAction:(id)sender {
    
    UIButton *btn = (UIButton*)sender;
    [self performSegueWithIdentifier:@"TagsSegue" sender:btn];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
   UIButton *btn = (UIButton*)sender;
    
    if([[segue identifier] isEqualToString:@"TagsSegue"]) {
        
        FLSearchResultsViewController *vc = [segue destinationViewController];
        vc.searchString = [self.tagsArray[btn.tag] objectForKey:@"_content"];
        vc.isTag = YES;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
