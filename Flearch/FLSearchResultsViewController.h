//
//  FLSearchResultsViewController.h
//  Flearch
//
//  Created by Christos  Bimpas on 10/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomTextField.h"

@interface FLSearchResultsViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate, UIPickerViewDataSource,UIPickerViewDelegate, UITextFieldDelegate>

@property(strong,nonatomic) NSString *searchString;
@property(assign) BOOL isTag;
@property(strong,nonatomic) UIActivityIndicatorView *myActivityIndicator;
@property(strong,nonatomic) UICollectionView *myCollectionView;
@property(strong,nonatomic) NSMutableArray *largePhotoUrlsArray;
@property(strong,nonatomic) NSMutableArray *mediumPhotoUrlsArray;
@property(strong,nonatomic) NSMutableArray *smallPhotoUrlsArray;
@property(strong,nonatomic) NSMutableArray *photoIdsArray;
@property(strong,nonatomic) UIButton *gridButton;
@property(strong,nonatomic) UIButton *listButton;
@property(strong,nonatomic) CustomTextField *sortTextField;
@property(strong,nonatomic) UIPickerView *myPickerView;
@property(strong,nonatomic) UIToolbar *myToolbar;
@property(strong,nonatomic) NSArray *sortTypesArray;
@property(strong,nonatomic) NSMutableDictionary *sortTypesKeysDictionary;
@property(strong,nonatomic) NSString *sortType;
@property(strong,nonatomic) NSString *sortText;
@property(strong,nonatomic) NSString *pageNoString;
@property(assign) float cellSize;
@property(assign) BOOL isListView;
@end
