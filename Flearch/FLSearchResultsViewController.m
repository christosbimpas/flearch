//
//  FLSearchResultsViewController.m
//  Flearch
//
//  Created by Christos  Bimpas on 10/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import "FLSearchResultsViewController.h"
#import "FlickrKit.h"
#import "global.h"
#import "CustomCollectionViewCell.h"
#import "FLPhotoDetailsViewController.h"


#define ITEMS_PAGE_SIZE 9
#define ITEM_CELL_IDENTIFIER @"ItemCell"
#define LOADING_CELL_IDENTIFIER @"LoadingItemCell"

@interface FLSearchResultsViewController () <UIViewControllerTransitioningDelegate>

@end

@implementation FLSearchResultsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initisialiseProperties];
    [self createUI];
    [self beforeTheSearch];
    [self doTheSearch];
    
    
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //code to be executed on the main queue after delay
        if([self.smallPhotoUrlsArray count]==0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your search didn't return any results!"  delegate:nil cancelButtonTitle:@"OK"otherButtonTitles: nil];
            [alert show];
            [self.navigationController popViewControllerAnimated:YES];
            
            
        }
    });
   }

-(void)initisialiseProperties {
    self.isListView = NO;
    self.pageNoString = @"1";
    self.cellSize = (self.view.frame.size.width-40)/3.0;
    self.sortTypesArray = [[NSArray alloc] initWithObjects:@"Posted most recently",@"Posted less recently",@"Taken most recently",@"Taken less recently",@"Most interesting", @"Less interesting",@"Relevenace", nil];
    NSArray *tempArray = [[NSArray alloc] initWithObjects:@"date-posted-desc",@"date-posted-asc",@"date-taken-desc",@"date-taken-asc",@"interestingness-desc", @"interestingness-asc",@"relevenace", nil];

    self.sortTypesKeysDictionary = [[NSMutableDictionary alloc] init];
    for(int i=0;i<[self.sortTypesArray count];i++) {
        
        [self.sortTypesKeysDictionary setValue:tempArray[i] forKey:self.sortTypesArray[i]];
    }
    
    self.sortType = tempArray[0];
    self.sortText = self.sortTypesArray[0];
                           
    
}



-(void)setGridView {
    
    self.isListView = NO;
    self.cellSize = (self.view.frame.size.width-40)/3.0;
    [self.myCollectionView reloadData];


}

-(void)setListView {
    
    self.isListView = YES;
    self.cellSize = self.view.frame.size.width-20;
    [self.myCollectionView reloadData];
    
    
}


-(void)createUI {
    
    UIImageView *myImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    myImageView.image = [UIImage imageNamed:@"headerImage"];
    myImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = myImageView;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-4],
                                    NSFontAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.tintColor = NAVIGATION_BACK_BUTTON_COLOR;
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor = NAVIGATION_COLOR;
    
    self.view.backgroundColor = GENERIC_BG_COLOR;
    self.myActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.myActivityIndicator.center = self.view.center;
    self.myActivityIndicator.layer.zPosition = 1000;
    
    [self.view addSubview:self.myActivityIndicator];
   
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    self.myCollectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(10, 64+20+25, self.view.frame.size.width-20, self.view.frame.size.height-64-55) collectionViewLayout:layout];
    [self.myCollectionView setDataSource:self];
    [self.myCollectionView setDelegate:self];
    [self.myCollectionView setBackgroundColor:[UIColor clearColor]];
    [self.myCollectionView registerClass:[CustomCollectionViewCell class] forCellWithReuseIdentifier:ITEM_CELL_IDENTIFIER];
    [self.myCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:LOADING_CELL_IDENTIFIER];

    [self.view addSubview:self.myCollectionView];
    
    self.gridButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 73, 25, 25)];
    [self.gridButton setBackgroundImage:[UIImage imageNamed:@"gridViewButton"] forState:UIControlStateNormal];
    self.gridButton.layer.borderWidth = 0.0;
    self.gridButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [self.gridButton addTarget:self action:@selector(setGridView) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.gridButton];
    
    self.listButton = [[UIButton alloc] initWithFrame:CGRectMake(45, 73, 25, 25)];
    [self.listButton setBackgroundImage:[UIImage imageNamed:@"listViewButton"] forState:UIControlStateNormal];
    self.listButton.layer.borderWidth = 0.0;
    self.listButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [self.listButton addTarget:self action:@selector(setListView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.listButton];
   
    
    
    self.myPickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-120, self.view.frame.size.width, 120)];
    self.myPickerView.delegate = self;
    self.myPickerView.dataSource = self;
    
    self.myToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.myToolbar.barStyle = UIBarStyleDefault;
    UIBarButtonItem *leftCountryButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelPickerAction)];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *rightCountryButton = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(donePickerAction)];
    [self.myToolbar setItems:[NSArray arrayWithObjects:leftCountryButton,flex,rightCountryButton, nil]];
    self.myToolbar.userInteractionEnabled = YES;
    
    UIView *textFieldBgView = [[UIView alloc] initWithFrame:CGRectMake(80, 73, self.view.frame.size.width - 90, 25)];
    textFieldBgView.layer.borderWidth = 1;
    textFieldBgView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    [self.view addSubview:textFieldBgView];
    
    self.sortTextField = [[CustomTextField alloc] initWithFrame:CGRectMake(80, 75, self.view.frame.size.width - 90, 20)];
    self.sortTextField.text = self.sortTypesArray[0];
    self.sortTextField.layer.zPosition = 1000000;
    self.sortTextField.textAlignment = NSTextAlignmentCenter;
    self.sortTextField.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-5];
    self.sortTextField.delegate = self;
    self.sortTextField.inputView = self.myPickerView;
    self.sortTextField.inputAccessoryView = self.myToolbar;
    [self.view addSubview:self.sortTextField];
    
    
    
}

-(void)removeObjectsFromArrays {
    
        [self.photoIdsArray removeAllObjects];
        [self.largePhotoUrlsArray removeAllObjects];
        [self.mediumPhotoUrlsArray removeAllObjects];
        [self.smallPhotoUrlsArray removeAllObjects];
}
#pragma mark SEARCH
-(void)beforeTheSearch {
    [self.myActivityIndicator startAnimating];
}



-(void)afterTheSearch {
    
    [self.myActivityIndicator stopAnimating];
    
   
}

-(void)doTheSearch {
    
    FKFlickrPhotosSearch *search = [[FKFlickrPhotosSearch alloc] init];
    search.sort = self.sortType;
    if(self.isTag) {
        search.tags = self.searchString;
    }
    else {
    search.text = self.searchString;
    }
    search.per_page =[NSString stringWithFormat:@"%d",ITEMS_PAGE_SIZE];
    search.page = self.pageNoString;
    [[FlickrKit sharedFlickrKit] call:search completion:^(NSDictionary *response, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (response) {
                
               
                [self afterTheSearch];
                if(self.largePhotoUrlsArray==nil) {
                    self.largePhotoUrlsArray = [[NSMutableArray alloc] init];
                }
                
                if(self.mediumPhotoUrlsArray==nil) {
                self.mediumPhotoUrlsArray = [[NSMutableArray alloc] init];
                }
                
                
                if(self.smallPhotoUrlsArray==nil) {
                    self.smallPhotoUrlsArray = [[NSMutableArray alloc] init];
                    
                }
                
                if(self.photoIdsArray==nil) {
                    self.photoIdsArray = [[NSMutableArray alloc] init];
                    
                }
                for (NSDictionary *photoDictionary in [response valueForKeyPath:@"photos.photo"]) {
                    NSURL *urlLarge = [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeMedium800 fromPhotoDictionary:photoDictionary];
                    NSURL *urlMedium = [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeMedium640 fromPhotoDictionary:photoDictionary];
                     NSURL *urlSmall = [[FlickrKit sharedFlickrKit] photoURLForSize:FKPhotoSizeSmall320 fromPhotoDictionary:photoDictionary];
                    NSString *imageId = [photoDictionary objectForKey:@"id"];
                    [self.smallPhotoUrlsArray addObject:urlSmall];
                    [self.mediumPhotoUrlsArray addObject:urlMedium];
                    [self.largePhotoUrlsArray addObject:urlLarge];
                    [self.photoIdsArray addObject:imageId];
                    
                }
                
                

                [self.myCollectionView reloadData];
               
                
                
            } else {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
                 [self.navigationController popViewControllerAnimated:YES];
            }
        });
        
    }];
    
   
}

#pragma mark REFINE

-(void)refineButtonAction {
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    UIViewController  *vc = [mainStoryboard instantiateViewControllerWithIdentifier: @"FLRefineId"];
    vc.transitioningDelegate = self;
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:NULL];

    
}
#pragma mark SORT
-(void)chooseSortTypeAction {
    
    
}

#pragma mark COLLECTION VIEW



- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    
    
    return [self.mediumPhotoUrlsArray count]+1;
    
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.item < [self.mediumPhotoUrlsArray count]) {
        
      
        if(indexPath.item == ([self.mediumPhotoUrlsArray count] - ITEMS_PAGE_SIZE + 1)){
            [self fetchMoreItems];
        }
        
        return [self itemCellForIndexPath:indexPath];
    } else {
        [self fetchMoreItems];
        return [self loadingCellForIndexPath:indexPath];
    }
}

- (UICollectionViewCell *)itemCellForIndexPath:(NSIndexPath *)indexPath {
    
    
        CustomCollectionViewCell *cell=(CustomCollectionViewCell *)[self.myCollectionView dequeueReusableCellWithReuseIdentifier:@"ItemCell"
                                                                                                             forIndexPath:indexPath];
    
    
    
        if(self.isListView) {
            [cell setCellWithImageUrl:self.mediumPhotoUrlsArray[indexPath.row] size:self.cellSize andGenericImage:@"genericImage"];
          
        }
    
        else {
             [cell setCellWithImageUrl:self.smallPhotoUrlsArray[indexPath.row] size:self.cellSize andGenericImage:@"genericSmallImage"];
            
        }
        


   
   
    
    return cell;
}

- (UICollectionViewCell *)loadingCellForIndexPath:(NSIndexPath *)indexPath {
    
    CustomCollectionViewCell *cell=(CustomCollectionViewCell *)[self.myCollectionView dequeueReusableCellWithReuseIdentifier:LOADING_CELL_IDENTIFIER
                                                                                                                forIndexPath:indexPath];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    return cell;
}



- (void)fetchMoreItems {
    DLog(@"FETCHING MORE ITEMS");
    
    self.pageNoString = [NSString stringWithFormat:@"%d",[self.pageNoString intValue]+1];
    [self doTheSearch];

}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(self.cellSize,self.cellSize);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    

    [self performSegueWithIdentifier:@"PhotoDetailsSegue" sender:indexPath];
    
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    
    if([[segue identifier] isEqualToString:@"PhotoDetailsSegue"]) {
        
        FLPhotoDetailsViewController *vc = [segue destinationViewController];
        vc.photoId = self.photoIdsArray[indexPath.row];
        vc.photoUrl = self.largePhotoUrlsArray[indexPath.row];
        
    }
}

#pragma mark PICKER VIEW DELEGATE

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
   
        
        return [self.sortTypesArray count];
   
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
   
        return self.sortTypesArray[row];
   
}

-(void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.sortTextField.text = self.sortTypesArray[row];
    
}

-(void)donePickerAction {
    
    if(![self.sortTextField.text isEqualToString:self.sortText]) {
    self.sortType = [self.sortTypesKeysDictionary objectForKey:self.sortTextField.text];
    self.sortText = self.sortTextField.text;
    [self removeObjectsFromArrays];
    [self beforeTheSearch];
    [self doTheSearch];
    }
    [self.sortTextField resignFirstResponder];
    
}

-(void)cancelPickerAction {
    
    self.sortTextField.text = self.sortText;
    [self.sortTextField resignFirstResponder];
    
}

#pragma mark TEXTFIELD DELEGATE


-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    self.sortTextField.text = self.sortText;
    DLog(@"textfield %@",self.sortTextField.text);
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
