//
//  FLSearchViewController.m
//  Flearch
//
//  Created by Christos  Bimpas on 10/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import "FLSearchViewController.h"
#import "global.h"
#import "FLSearchResultsViewController.h"
@interface FLSearchViewController ()

@end

@implementation FLSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = GENERIC_BG_COLOR;
    
   
    [self createUI];
    
    [self.searchTextField becomeFirstResponder];
}

-(void)createUI {
    
    
    UIImageView *myImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    myImageView.image = [UIImage imageNamed:@"headerImage"];
    myImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = myImageView;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-4],
                                    NSFontAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.tintColor = NAVIGATION_BACK_BUTTON_COLOR;
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor = NAVIGATION_COLOR;
    
    
    self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(20,  90, self.view.frame.size.width-40, 25)];
    
    self.searchTextField.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-2];
    self.searchTextField.delegate = self;
    self.searchTextField.placeholder = @"Type here";
    self.searchTextField.textAlignment = NSTextAlignmentCenter;
    self.searchTextField.returnKeyType = UIReturnKeySearch;
    self.searchTextField.borderStyle = UITextBorderStyleLine;
     self.searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.view addSubview:self.searchTextField];
    
    
    self.searchButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 140, self.view.frame.size.width-40, 40)];
    [self.searchButton setBackgroundColor:BUTTON_BG_COLOR];
    [self.searchButton setTitle:@"Search" forState:UIControlStateNormal];
    self.searchButton.titleLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE];
    [self.searchButton setTitleColor:BUTTON_TITLE_COLOR forState:UIControlStateNormal];
    [self.searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.searchButton];
    
    
    
}

-(void)searchAction {
    
    if(self.searchTextField.text.length==0) {
       
        UIAlertController *myAlertController = [UIAlertController alertControllerWithTitle:@"Oops"
                                                                                   message: @"Please type something."
                                                                            preferredStyle:UIAlertControllerStyleAlert                   ];
        
      
        UIAlertAction* ok = [UIAlertAction
                             actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 //Do some thing here, eg dismiss the alertwindow
                                 [myAlertController dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
        
        //Step 3: Add the UIAlertAction ok that we just created to our AlertController
        [myAlertController addAction: ok];
        
        //Step 4: Present the alert to the user
        [self presentViewController:myAlertController animated:YES completion:nil];
    }
    
    else {
        
        [self performSegueWithIdentifier:@"SearchSegue" sender:nil];
        
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
   
    if([[segue identifier] isEqualToString:@"SearchSegue"]) {
        
        FLSearchResultsViewController *vc = [segue destinationViewController];
        vc.searchString = self.searchTextField.text;
        vc.isTag = NO;
       
    }
}

#pragma mark - TextField Delegates


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self searchAction];
    
    return  YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
