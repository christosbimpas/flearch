//
//  ViewController.h
//  Flearch
//
//  Created by Christos  Bimpas on 10/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickrKit.h"

@interface FLViewController : UIViewController <UIScrollViewDelegate>

@property (strong,nonatomic) UIButton *getStartedButton;
@property (strong, nonatomic)  UIView *circlesMainView;
@property (strong, nonatomic)  UIScrollView *imageScrollView;
@property(strong,nonatomic) NSArray *imagesArray;
@end

