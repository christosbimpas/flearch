//
//  ViewController.m
//  Flearch
//
//  Created by Christos  Bimpas on 10/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#import "FLViewController.h"
#import "global.h"





@interface FLViewController ()

@end

@implementation FLViewController {
    
    
    int circlesCount;
     int imageNo;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    imageNo = 0;
    self.view.backgroundColor = GENERIC_BG_COLOR;

    self.getStartedButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50)];
    [self.getStartedButton setBackgroundColor:BUTTON_BG_COLOR];
    [self.getStartedButton setTitle:@"Get Started" forState:UIControlStateNormal];
    [self.getStartedButton setTitleColor:BUTTON_TITLE_COLOR forState:UIControlStateNormal];
    self.getStartedButton.titleLabel.font = [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE];
    [self.getStartedButton addTarget:self action:@selector(getStartedAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.getStartedButton];
    
    
    UIImageView *myImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 60, 60)];
    myImageView.image = [UIImage imageNamed:@"headerImage"];
    myImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.navigationItem.titleView = myImageView;
    
    NSDictionary *textAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont fontWithName:FONT_LIGHT size:FONT_LIGHT_SIZE-4],
                                    NSFontAttributeName, nil];
    self.navigationController.navigationBar.titleTextAttributes = textAttributes;
    self.navigationController.navigationBar.tintColor = NAVIGATION_BACK_BUTTON_COLOR;
    [self.navigationItem.leftBarButtonItem setTitleTextAttributes:textAttributes forState:UIControlStateNormal];
    self.navigationController.navigationBar.barTintColor = NAVIGATION_COLOR;
   
    self.imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-self.getStartedButton.frame.size.height-30)];
    self.imageScrollView.delegate = self;
    self.imageScrollView.pagingEnabled = YES;
    self.imageScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    [self.view addSubview:self.imageScrollView];
    
    self.imagesArray = [[NSArray alloc] initWithObjects:@"start0",@"start1", nil];
    for (int i=0; i<[self.imagesArray count]; i++) {
       
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((i)*self.imageScrollView.frame.size.width, 0, self.imageScrollView.frame.size.width, self.imageScrollView.frame.size.height)];
        imgV.image = [UIImage imageNamed:[NSString stringWithFormat:@"start%d",i]];
        imgV.contentMode=UIViewContentModeScaleAspectFill | UIViewContentModeBottom;
        // apply tag to access in future
        imgV.tag=i+1;
        // add to scrollView
        [self.imageScrollView addSubview:imgV];
    }
    
    // set the content size to 10 image width
    [self.imageScrollView setContentSize:CGSizeMake(self.imageScrollView.frame.size.width*[self.imagesArray count], self.imageScrollView.frame.size.height)];
    
    self.circlesMainView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-self.getStartedButton.frame.size.height-30, self.view.frame.size.width, 30)];
    self.circlesMainView.backgroundColor = [UIColor colorWithRed:35/255.0 green:35/255.0 blue:35/255.0 alpha:1.0];
     [self createCircles];
    [self.view addSubview:self.circlesMainView];
    self.circlesMainView.layer.zPosition = 1000;
}


-(void)createCircles {
    
    for (UIView* b in self.circlesMainView.subviews)
    {
        [b removeFromSuperview];
    }
    
    int circleDiameter = 10;
    int x = self.view.frame.size.width/2.0 - (circleDiameter/2)*[self.imagesArray count];
    
    
        for (int i=0;i<[self.imagesArray count];i++) {
            UIView *cirlcleView = [[UIView alloc] initWithFrame:CGRectMake(x,10,circleDiameter,circleDiameter)];
            cirlcleView.alpha = 1.0;
            cirlcleView.layer.cornerRadius = circleDiameter/2;
            if(i==imageNo) {
                cirlcleView.backgroundColor = [UIColor colorWithRed:209/255.0 green:58/255.0 blue:37/255.0 alpha:1.0];
            }
            else {
                cirlcleView.backgroundColor = [UIColor whiteColor];
                cirlcleView.alpha = 0.7;
            }
            
            [self.circlesMainView addSubview:cirlcleView];
            x = x +20;
            
            
        }
   
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    imageNo = scrollView.contentOffset.x / scrollView.frame.size.width;
    
    [self createCircles];
}



-(void)getStartedAction {
    
    [self performSegueWithIdentifier:@"GetStartedSegue" sender:nil];

}

-(BOOL)prefersStatusBarHidden {
    
    return NO;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
