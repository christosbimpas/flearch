//
//  global.h
//  Flearch
//
//  Created by Christos  Bimpas on 10/11/2015.
//  Copyright © 2015 Christos  Bimpas. All rights reserved.
//

#ifndef global_h
#define global_h

#ifdef DEBUG
#define DLog( s, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define DLog( s, ... )
#endif


#define BUTTON_TITLE_COLOR [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]
#define BUTTON_BG_COLOR [UIColor colorWithRed:80/255.0 green:160/255.0 blue:161/255.0 alpha:1.0]
#define BUTTON_BG_REVERSE_COLOR [UIColor colorWithRed:41/255.0 green:49/255.0 blue:158/255.0 alpha:1.0]
#define GENERIC_BG_COLOR [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0]
#define BUTTON_TITLE_DARK_COLOR [UIColor colorWithRed:22/255.0 green:22/255.0 blue:22/255.0 alpha:1.0]
#define PHOTO_DETAILS_BG_COLOR [UIColor colorWithRed:22/255.0 green:22/255.0 blue:22/255.0 alpha:1.0]
#define FERTILIZER_REMINDER [UIColor colorWithRed:0/255.0 green:230/255.0 blue:154/255.0 alpha:1.0]

#define NAVIGATION_COLOR [UIColor colorWithRed:204/255.0 green:204/255.0 blue:204/255.0 alpha:1.0]
#define NAVIGATION_BACK_BUTTON_COLOR [UIColor colorWithRed:80/255.0 green:160/255.0 blue:161/255.0 alpha:1.0]
#define FONT_LIGHT @"SFUIDisplay-Light"

#define FONT_LIGHT_SIZE 20

#define FONT_BOLD @"SFUIText-Bold"

#define FONT_BOLD_SIZE 20




#endif /* global_h */
